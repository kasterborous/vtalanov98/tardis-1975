local PART={}
PART.ID = "bakerlever3"
PART.Name = "1975 TARDIS Lever 3"
PART.Model = "models/doctorwho1200/baker/lever3.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2.0

if SERVER then
	function PART:Draw()
		self:DrawModel()
	end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/baker/lever.wav" ))
		self.exterior:ToggleFlight()
	end
end

TARDIS:AddPart(PART,e)