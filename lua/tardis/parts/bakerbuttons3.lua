local PART={}
PART.ID = "bakerbuttons3"
PART.Name = "1975 Baker Buttons 3"
PART.Model = "models/doctorwho1200/baker/buttons3.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/baker/button.wav" ))
	end
end

TARDIS:AddPart(PART)