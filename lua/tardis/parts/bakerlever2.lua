local PART={}
PART.ID = "bakerlever2"
PART.Name = "1975 TARDIS Lever 2"
PART.Model = "models/doctorwho1200/baker/lever2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate= true
PART.AnimateSpeed = 2.0

if SERVER then
	function PART:Draw()
		self:DrawModel()
	end
	function PART:Use(activator)
		self:EmitSound("doctorwho1200/baker/lever.wav")
		local exterior=self.exterior
			if exterior:GetData("vortex") then
				exterior:Mat()
			else
				exterior:Demat()
			end
	end
end

TARDIS:AddPart(PART,e)