local PART={}
PART.ID = "bakertoggle3"
PART.Name = "1975 TARDIS Toggle 3"
PART.Model = "models/doctorwho1200/baker/toggle3.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 3

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/baker/toggle.wav" ))
	end
end

TARDIS:AddPart(PART,e)