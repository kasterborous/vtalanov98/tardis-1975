local PART={}
PART.ID = "bakerlever1"
PART.Name = "1975 TARDIS Lever 1"
PART.Model = "models/doctorwho1200/baker/lever1.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 2

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/baker/lever.wav" ))
	end
end

TARDIS:AddPart(PART,e)