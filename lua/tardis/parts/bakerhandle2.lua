local PART={}
PART.ID = "bakerhandle2"
PART.Name = "1975 TARDIS Handle 2"
PART.Model = "models/doctorwho1200/baker/handle2.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 3

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/baker/handle.wav" ))
	end
end

TARDIS:AddPart(PART,e)