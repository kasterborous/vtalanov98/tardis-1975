local PART={}
PART.ID = "bakerbuttons6"
PART.Name = "1975 Baker Buttons 6"
PART.Model = "models/doctorwho1200/baker/buttons6.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/baker/buttons.wav" ))
	end
end

TARDIS:AddPart(PART)