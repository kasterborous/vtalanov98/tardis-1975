local PART={}
PART.ID = "bakerrot"
PART.Name = "1975 Baker Rotor"
PART.Model = "models/doctorwho1200/baker/rotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true

if CLIENT then
	function PART:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	end

	function PART:Think()
		local ext=self.exterior
		if (self.timerotor.pos>0 and ext:GetData("flight") or ext:GetData("teleport") or ext:GetData("vortex")) or (ext:GetData("flight") or ext:GetData("teleport") or ext:GetData("vortex")) then
			if self.timerotor.pos==1 then
				self.timerotor.mode=0
			elseif self.timerotor.pos==0 and (ext:GetData("flight") or ext:GetData("teleport") or ext:GetData("vortex")) then
				self.timerotor.mode=1
			end
			
			self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.3 )
			self:SetPoseParameter( "rotor", self.timerotor.pos )
		end
	end
end

TARDIS:AddPart(PART)