local PART={}
PART.ID = "bakerhandle4"
PART.Name = "1975 TARDIS Handle 4"
PART.Model = "models/doctorwho1200/baker/handle4.mdl"
PART.AutoSetup = true
PART.Animate=true
PART.AnimateSpeed = 3

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/baker/handle.wav" ))
		self.exterior:ToggleDoor()
	end
end

TARDIS:AddPart(PART,e)