local PART={}
PART.ID = "bakergov"
PART.Name = "1975 TARDIS Governor"
PART.Model = "models/doctorwho1200/baker/governor.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 3

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/baker/governor.wav" ))
	end
end

TARDIS:AddPart(PART,e)