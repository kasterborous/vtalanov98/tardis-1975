local PART={}
PART.ID = "bakerswitch6"
PART.Name = "1975 TARDIS Switch 6"
PART.Model = "models/doctorwho1200/baker/switch6.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 4

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/baker/switch.wav" ))
	end
end

TARDIS:AddPart(PART,e)