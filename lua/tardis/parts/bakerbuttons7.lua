local PART={}
PART.ID = "bakerbuttons7"
PART.Name = "1975 Baker Buttons 7"
PART.Model = "models/doctorwho1200/baker/buttons7.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/baker/buttons.wav" ))
	end
end

TARDIS:AddPart(PART)