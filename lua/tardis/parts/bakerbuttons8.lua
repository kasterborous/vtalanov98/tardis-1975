local PART={}
PART.ID = "bakerbuttons8"
PART.Name = "1975 Baker Buttons 8"
PART.Model = "models/doctorwho1200/baker/buttons8.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
        function PART:Draw()
		self:DrawModel()
        end
	function PART:Use(activator)
		self:EmitSound( Sound( "doctorwho1200/baker/button.wav" ))
	end
end

TARDIS:AddPart(PART)